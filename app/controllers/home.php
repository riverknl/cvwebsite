<?php

class Home extends Controller {

  public function index() {
      $this->view('home/index');
  }

  public function test() {
    $model = $this->model('Gebruikers');
    $results = $model->show();

    if($results) {
      print_r($results);
      $this->view('home/test', $results);
    } else {
      $this->view('errors/error_1');
    }
  }

}
?>
