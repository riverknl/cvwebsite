<?php
// https://www.wikihow.com/Create-a-Secure-Login-Script-in-PHP-and-MySQL
class Account extends Controller {

  public function aanmelden() {
    if(!is_ingelogd()) {
      if($_SERVER["REQUEST_METHOD"] == 'POST') {
        $model =   $this->model('accountModel');

        $aanmelden = $model->aanmelden($_POST);

        if($aanmelden) {
          $this->view('account/aanmelden', 'succesvol');

          // hier nog model aanroepen om activatie link te sturen
        } else {
          $return = array_push($_POST, $aanmelden);
          $this->view('account/aanmelden', $_POST);
        }

      } else {
        $this->view('account/aanmelden');
      }
    } else {
      is_ingelogd_redirect();
    }
  }

  public function activeren($email = '', $token = '') {
    // check of email en token in de url voorkomen
  }

  public function inloggen() {
    if(!is_ingelogd()) {
      $model = $this->model('accountModel');

      if($_SERVER["REQUEST_METHOD"] == "POST") {
        $inloggen = $model->inloggen($_POST);
        $actief = $model->account_actief($_POST['email']);

        if($inloggen) {
          if($actief) {
            $user_browser = $_SERVER['HTTP_USER_AGENT'];
            $gebruikersid = preg_replace("/[^0-9]+/", "", $inloggen['gebruikersid']);
            $_SESSION['gebruikersid'] = $gebruikersid;
            $_SESSION['email'] = $inloggen['email'];
            $_SESSION['login_string'] = hash('sha512', $inloggen['wachtwoord'] . $user_browser);

            is_ingelogd_redirect();
          } else {
            array_push($_POST, 'error_3');
            $this->view('account/inloggen', $_POST);
          }
        } else {
          array_push($_POST, 'error_2');
          $this->view('account/inloggen', $_POST);
        }
      }
      $this->view('account/inloggen');
    } else {
      is_ingelogd_redirect();
    }
  }

  public function wachtwoordvergeten() {
    $model = $this->model('accountModel');

    if($_SERVER["REQUEST_METHOD"] == "POST") {
      $vergeten = $model->wachtwoord_vergeten($_POST);

      if($vergeten) {
        // push aanmaken met token
        $push = [];
        $push['gebruikersid'] = $vergeten['gebruikersid'];
        $push['token'] = genereer_token();
        $model->wachtwoord_reset_token($push);

        // email + token ophalen
        $get_gegevens = $model->wachtwoord_reset_get($vergeten['gebruikersid']);

        // hier dan nog een mail sturen met link naar email+token;

        $this->view('account/wachtwoordvergeten', 'succesvol');
      } else {

        $this->view('account/wachtwoordvergeten', 'error_3');
      }
    } else {
      $this->view('account/wachtwoordvergeten');
    }
  }

  public function wachtwoordresetten($token = '', $email = '') {
    $model = $this->model('accountModel');

    if(!empty($token) && !empty($email)) {
      $push_array = [];
      $push_array[] = $token;
      $push_array[] = $email;

    //  print_r($push);
      // doe de database check
      $push = $model->wachtwoord_reset_check($push_array);

      if($push) {
        if($_SERVER["REQUEST_METHOD"] == 'POST') {
          array_push($push, $_POST);
          //print_r($push);
          $reset_wachtwoord = $model->wachtwoord_reset($push);

          if(empty($reset_wachtwoord)) {
              $this->view('account/wachtwoordresetten', 'succesvol');
          } else {
            array_push($push, 'error_2');
            //print_r($push);
            $this->view('account/wachtwoordresetten', $push);
          }
        } else {
          $this->view('account/wachtwoordresetten', $push);
        }
      } else {
        print_r('false');
      }
      //print_r($token . ' ' . $email);

    } else {
      niet_ingelogd_redirect();
    }
  }

  public function uitloggen() {

    if(is_ingelogd()) {
      $_SESSION = array();

      $params = session_get_cookie_params();

      setcookie(session_name(),
        '', time() - 42000,
        $params["path"],
        $params["domain"],
        $params["secure"],
        $params["httponly"]);

      session_destroy();
      $this->view('account/succesvol-uitgelogd');
    } else {
      niet_ingelogd_redirect();
    }
  }

  public function mijnaccount() {
    $this->view('account/mijnaccount');
  }
}

?>
