<section class="content bg-grijs align-center">
  <h1>Inloggen</h1>
  <?php if(isset($data[0]) && $data[0] == 'error_2'){ ?>
    <p class="warning">E-mail en wachtwoord komen niet overeen.</p>
  <?php } ?>
  <?php if(isset($data[0]) && $data[0] == 'error_3'){ ?>
    <p class="warning">Je account is nog niet geactiveerd!</p>
  <?php } ?>
  <form method="POST" action="<?=URL?>/account/inloggen">
    <input placeholder="E-mailadres" type="text" name="email" value="<?= (isset($data['email']) ? $data['email'] : '')  ?>" required/><br />
    <input placeholder="Wachtwoord" type="password" name="wachtwoord" value=""  required/><br />
    <input type="submit" value="Inloggen"><br />
    <a href="<?=URL?>/account/wachtwoordvergeten">Wachtwoord vergeten?</a>
  </form>
</section>
