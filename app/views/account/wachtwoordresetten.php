<section class="content bg-grijs ">
  <h1>Wachtwoord resetten</h1>
  <?php if($data == 'succesvol') {?>
    Wachtwoord succesvol gereset (Y).
  <?php } else {?>
  <?php if(isset($data[1]) && $data[1] == 'error_2'){ ?>
    <p>Wachtwoorden komen niet overeen.</p>
  <?php } ?>
    <form method="POST" action="<?=URL?>/account/wachtwoordresetten/<?=$data['email']?>/<?=$data['wachtwoord_token']?>">
      <input placeholder="Wachtwoord" type="password" name="wachtwoord" value="" required/>
      <input placeholder="Herhaal wachtwoord" type="password" name="wachtwoord2" value="" required/>
      <input type="submit" value="Reset wachtwoord">
    </form>
  <?php } ?>
  </section>
