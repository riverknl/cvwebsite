<section class="content bg-grijs  align-center">
  <h1>Aanmelden</h1>
  <?php if($data == 'succesvol') {?>
    Je bent succesvol aangemeld. Je ontvangt zo snel mogelijk een e-mail waarmee je je account kunt bevestigen.
  <?php } else {?>
  <?php
  if(isset($data[0])) {
    if(isset($data[0]['email_error'])) {
  ?>
    <p>Geen geldig email adres.</p>
  <?php
    }
    if(isset($data[0]['wachtwoord_error'])) {
  ?>
    <p>Wachtwoorden komen niet overeen.</p>
  <?php
    }
    if(isset($data[0]['email_double_error'])) {
      ?>
      <p>Dit e-mail adres is al in gebruik, voer een ander e-mail adres in of <a href="<?=URL?>/account/wachtwoordvergeten">reset je wachtwoord</a>.</p>
      <?php
    }
  }
  ?>
  <form method="POST" action="<?=URL?>/account/aanmelden">
    <input placeholder="E-mailadres" type="text" name="email" value="<?= (isset($data['email']) ? $data['email'] : '')  ?>" required/><br />
    <input placeholder="Wachtwoord" type="password" name="wachtwoord" value="" required/><br />
    <input placeholder="Herhaal wachtwoord" type="password" name="wachtwoord2" value="" required/><br />
    <input type="submit" value="Aanmelden">
  </form>
<?php } ?>
</section>
