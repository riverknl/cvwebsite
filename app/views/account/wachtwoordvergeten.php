<section class="content bg-grijs align-center">
  <h1>Wachtwoord vergeten</h1>
  <?php if($data == 'succesvol' OR $data == 'error_3') {?>
    <p>Er is een link verstuurd naar het opgegeven e-mailadres om het wachtwoord te resetten.</p>
  <?php } else {?>
  <form method="POST" action="<?=URL?>/account/wachtwoordvergeten">
    <input placeholder="E-mailadres" type="text" name="email" value="" required/><br />
    <input type="submit" value="Reset wachtwoord">
  </form>
<?php } ?>
</section>
