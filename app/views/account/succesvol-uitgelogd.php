<section class="content bg-grijs t">
  <h1>Succesvol uitgelogd</h1>
  <p>Je bent succesvol uitgelogd en wordt automatisch doorgestuurd naar de homepagina. Gebeurt dit niet? <a href="<?=URL?>">Klik dan hier</a>.</p>
</section>
