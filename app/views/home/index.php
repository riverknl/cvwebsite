<div class="top">
<?php if(is_ingelogd()) {?>
  <section class="content align-center">
    <h1>Welkom <?php echo $_SESSION['email'] ?></h1>
    <a href="#" class="hero-button">Maak nu jouw cv aan!</a>
  </section>
  <?php } else {?>
    <section class="content bg-grijs hero align-center">
      <h1>Ben jij ook toe aan een nieuw CV?</h1>
      <p>Met keuze uit 25+ designs val jij gegarandeerd op tijdens het soliciteren.
    Personaliseer je CV en spring eruit! Meld je nu gratis aan en
    start met het maken van jouw CV.</p>
      <a href="<?=URL?>/account/aanmelden" class="hero-button">Ja, ik wil ook een nieuw CV!</a>
    </section>

    <section class="content align-center">
      <h1>Hoe werkt het?</h1>
      <p>Het is heel simpel, meld je hier aan, log in en je kan meteen beginnen.
        Vul eerst je personalia in. Daarna je werkervaring en opleidingen.
         Oja, vergeet ook je profiel niet in te vullen. Dit is een kort stukje
         waar je over jezelf wat vertelt. Alles ingevuld? Kies als laatst een
         template en bekijk hoe jouw CV eruit komt te zien. Als je tevreden ben is
         je CV klaar en kan je hem downloaden.</p>
      <a href="<?=URL?>/account/aanmelden" class="hero-button">Ja, ik wil ook een nieuw CV!</a>
    </section>
  <?php } ?>
</div>
