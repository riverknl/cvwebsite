<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="<?=URL?>/css/minireset.css">
  <link rel="stylesheet" href="<?=URL?>/css/main.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body>
<div class="wrapper">
  <header>
    <div class="site-title">
      <a href="<?=URL?>">Website Naam</a>
    </div>
    <nav>

      <?php
      if(is_ingelogd()) {?>
        <ul class="navigation">
          <li><a href="<?=URL .'/account/uitloggen'?>">Uitloggen</a></li>
          <li><a href="<?=URL .'/account/mijnaccount'?>">Mijn account</a></li>
        </ul>
      <?php } else {?>
        <ul class="navigation">
          <li><a href="<?=URL .'/account/aanmelden'?>">Aanmelden</a></li>
          <li><a href="<?=URL .'/account/inloggen'?>">Inloggen</a></li>
        </ul>
      <?php }
      ?>

    </nav>

  </header>
