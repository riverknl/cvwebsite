<?php
class AccountModel extends Model{

  public function aanmelden($data = []) {
    //print_r($data);
    $error = [];
    $data['email'] = strtolower($data['email']);
    if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
      $error['email_error'] = 1;
    }
    if($data['wachtwoord'] != $data['wachtwoord2']) {
      $error['wachtwoord_error'] = 1;
    }
    if($this->aanmelden_check_email($data['email']) == true) {
      $error['email_double_error'] = '1';
    }

    if(empty($error)) {
      $hashwachtwoord = password_hash($_POST['wachtwoord'], PASSWORD_BCRYPT);
      $token = genereer_token();
      $connect = $this->db->connect();
      $sql = "INSERT INTO gebruikers (email,wachtwoord, activatie_code, actief) VALUES (:email, :wachtwoord, :activatie_code, 0)";
      $query = $connect->prepare($sql);
      $query->bindParam(":email", $_POST['email']);
      $query->bindParam(":wachtwoord", $hashwachtwoord);
      $query->bindParam(":activatie_code", $token);
      return $query->execute();

    } else {
      return $error;
    }

  }

  public function aanmelden_check_email($data) {
    $data = strtolower($data);
    $connect = $this->db->connect();
    $sql = "SELECT email FROM gebruikers WHERE email=:email";
    $query = $connect->prepare($sql);
    $query->bindParam(":email", $data);
    $query->execute();

    $fetch = $query->fetch();

    if($fetch['email'] === $data) {
      return true;
    } else {
      return false;
    }
  }

  public function inloggen($data = []) {

    $data['email'] = strtolower($data['email']);

    $connect = $this->db->connect();
    $sql = "SELECT * FROM gebruikers WHERE email=:email";
    $query = $connect->prepare($sql);
    $query->bindParam(":email", $data['email']);
    $query->execute();

    $fetch = $query->fetch();

    $checkwachtwoord = password_verify($data['wachtwoord'], $fetch['wachtwoord']);

    if($checkwachtwoord) {
        return $fetch;
    }
  }

  public function account_actief($data) {
    $data = strtolower($data);
    $connect = $this->db->connect();
    $sql = "SELECT * FROM gebruikers WHERE email=:email AND actief=1";
    $query = $connect->prepare($sql);
    $query->bindParam(":email", $data);
    $query->execute();

    $fetch = $query->fetch();

    if($fetch) {
      return true;
    } else {
      return false;
    }

  }

  public function wachtwoord_vergeten($data = []) {
    $data['email'] = strtolower($data['email']);
    $connect = $this->db->connect();
    $sql = "SELECT * FROM gebruikers WHERE email=:email";
    $query = $connect->prepare($sql);
    $query->bindParam(":email", $data['email']);
    $query->execute();

    return $query->fetch();
  }

  public function wachtwoord_reset_token($data = []) {

    $token = genereer_token();

    $connect = $this->db->connect();
    $sql = "UPDATE gebruikers SET wachtwoord_token=:wachtwoord_token  WHERE gebruikersid=:gebruikersid";
    $query = $connect->prepare($sql);
    $query->bindParam(":gebruikersid", $data['gebruikersid']);
    $query->bindParam(":wachtwoord_token", $token);
    $query->execute();
    $count = $query->rowCount();

    if($count >= 0) {
      return $token;
    } else {
      return false;
    }
  }

  public function wachtwoord_reset_get($data) {
    $connect = $this->db->connect();
    $sql = "SELECT * FROM gebruikers WHERE gebruikersid=:id";
    $query = $connect->prepare($sql);
    $query->bindParam(":id", $data);
    $query->execute();

    return $query->fetch();
  }

  public function wachtwoord_reset($data = []) {

    $error = [];
    if($data[0]['wachtwoord'] != $data[0]['wachtwoord2']) {
      $error['wachtwoord_error'] = 1;
    }

    if(empty($error)) {

      $hashwachtwoord = password_hash($data[0]['wachtwoord'], PASSWORD_BCRYPT);

      $connect = $this->db->connect();
      $sql = "UPDATE gebruikers SET wachtwoord=:wachtwoord, wachtwoord_token='' WHERE gebruikersid=:gebruikersid; ";
      $query = $connect->prepare($sql);
      $query->bindParam(":gebruikersid", $data['gebruikersid']);
      $query->bindParam(":wachtwoord", $hashwachtwoord);
      $query->execute();

      return $error;
    } else {
      return $error;
    }
  }

  public function wachtwoord_reset_check($data = []) {
    $data[0] = strtolower($data[0]);

    $connect = $this->db->connect();
    $sql = "SELECT * FROM gebruikers WHERE email=:email AND wachtwoord_token=:token";
    $query = $connect->prepare($sql);
    $query->bindParam(":email", $data[0]);
    $query->bindParam(":token", $data[1]);
    $query->execute();

    $fetch = $query->fetch();

    if($fetch) {
      return $fetch;
    } else {
      return false;
    }
  }

  public function get_wachtwoord($id) {
    $connect = $this->db->connect();
    $sql = "SELECT wachtwoord FROM gebruikers WHERE gebruikersid=:id";
    $query = $connect->prepare($sql);
    $query->bindParam(":id", $id);
    $query->execute();

    $fetch = $query->fetch();

    return $fetch;
  }
}
?>
