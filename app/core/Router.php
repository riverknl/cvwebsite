<?php
class Router {

  protected $controller = '';
  protected $method = '';
  protected $params = [];

  public function __construct() {
      $uri = $this->parseUri();

      if(!$uri) {
        $this->controller = 'home';
        $this->method = 'index';

        require_once 'app/controllers/' . $this->controller .'.php';

        $this->controller = new $this->controller;

        call_user_func_array([$this->controller, $this->method], $this->params);

      } elseif(isset($uri) && $uri[0] == 'home' && empty($uri[1])) {
        $this->controller = 'home';
        $this->method = 'index';

        require_once 'app/controllers/' . $this->controller .'.php';

        $this->controller = new $this->controller;

        call_user_func_array([$this->controller, $this->method], $this->params);

      } elseif (file_exists('app/controllers/'.$uri[0].'.php')) {

        require_once 'app/controllers/'.$uri[0].'.php';

        $this->controller = $uri[0];

        $this->controller = new $this->controller;

        if(isset($uri[1])) {
          if(method_exists($this->controller, $uri[1])) {
            $this->method = $uri[1];

            unset($uri[0], $uri[1]);

            $this->params = $uri ? array_values($uri) : [];

            call_user_func_array([$this->controller, $this->method], $this->params);

          } else {
            $this->error_404();
          }
        } else {
          $this->error_404();
        }

      } else {
        $this->error_404();
      }
  }

  protected function error_404() {
    $this->controller = 'errors';
    $this->method = 'error_404';
    $this->params = [];

    require_once 'app/controllers/errors.php';

    $this->controller = new $this->controller;

    call_user_func_array([$this->controller, $this->method], $this->params);
  }

  public function parseUri() {
    if(isset($_GET['uri'])){
      return $uri = explode('/',filter_var(trim($_GET['uri'], '/'), FILTER_SANITIZE_URL));
    }
  }

}
?>
