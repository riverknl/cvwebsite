<?php
function is_ingelogd() {
  if(isset($_SESSION['gebruikersid'], $_SESSION['email'], $_SESSION['login_string'])) {

    $db = new Database;
    $gebruikersid = $_SESSION['gebruikersid'];
    $email = $_SESSION['email'];
    $login_string = $_SESSION['login_string'];

    $user_browser = $_SERVER['HTTP_USER_AGENT'];

    $connect = $db->connect();
    $sql = "SELECT wachtwoord FROM gebruikers WHERE gebruikersid=:id";
    $query = $connect->prepare($sql);
    $query->bindParam(":id", $_SESSION['gebruikersid']);
    $query->execute();

    $fetch = $query->fetch();

    if($fetch) {
      $login_check = hash('sha512', $fetch['wachtwoord'] . $user_browser);
        if($login_check === $login_string) {
          return true;
        } else {
          return false;
        }
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function is_ingelogd_redirect() {
  echo "<script type='text/javascript'>window.location.href = '" . URL . "';</script>";
  //header('location :' . URL );
  exit();
}

function niet_ingelogd_redirect() {
  echo "<script type='text/javascript'>window.location.href = '" . URL . "/account/inloggen';</script>";
  //header('location :' . URL . '/account/inloggen');
  exit();
}

function sec_session_start() {
  $session_name = 'sec_session_id';
  $secure = SECURE;
  $httponly = true;

  if(ini_set('session.use_only_cookies', 1) === FALSE) {
    header("Location: " . URL . "/errors/error_900");
     exit();
  }

  $cookieParams = session_get_cookie_params();
  session_name($session_name);
  session_start();
  session_regenerate_id();
}

function hash_wachtwoord() {

}

function verify_wachtwoord() {

}

function genereer_token() {
  $token = bin2hex(random_bytes(78));

  return $token;
}
?>
