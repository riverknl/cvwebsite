<?php

// Database instellingen
define('DB_TYPE', 'mysql');		// Wat voor type database gebruik je?
define('DB_HOST', 'localhost'); // Wat is het IP adres van de server (127.0.0.1 is de lokae machine)
define('DB_NAME', 'cvwebsite'); // Wat is de database naam
define('DB_USER', 'root'); 		// Wat is de database gebruiker
define('DB_PASS', 'verkerk123');			// Wat is het database wachtwoord
define('DB_CHARSET', 'utf8'); 	// Welke karakterset wordt gebruikt

define('URL_PROTOCOL', '//');			// Het URL protocol bepaalt of een site via HTTP of HTTPS wordt opgevraagd. Bij '//' wordt de gebruikte methode gebruikt
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);	// Dit bepaald de URL van de website
define('URL', URL_PROTOCOL . URL_DOMAIN ); // Dit genereerd de standaard URL van de applicatie
define('SECURE', true);
