CREATE TABLE `gebruikers` (
  `gebruikersid` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `wachtwoord` varchar(255) DEFAULT NULL,
  `aanmaak_datum` datetime DEFAULT NULL,
  `laatst_ingelogd` datetime DEFAULT NULL,
  `ipadres` varchar(255) DEFAULT NULL,
  `nieuwsbrief` int(11) DEFAULT '0',

) ENGINE=InnoDB DEFAULT CHARSET=utf8_bin;
